# RF24 Libraries Cleanup Project #
Each component of the _RF24_ set of libraries by _TMRh20_ will have a **CLEANUP.md** file, which shall contain notes
about what has been done to cleanup and/or improve the library.